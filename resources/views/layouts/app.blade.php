<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Festilove</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
<div class="container fluid">
    <div class="row container-header">
            <h1 id="main-banner" class="glow">Festilove</h1>
            @if(Route::has('login'))
                @auth
                <div class="nav-block">
                    <div class="nav-menu">
                        <a class="nav-link" href="{{ url('/home') }}">Profil</a>
                    </div>
                    <div class="nav-menu">
                        <a class="nav-link" href="{{ url('/logout') }}">Wyloguj</a>
                    </div>
                </div>

                @else
                @if (Route::has('register') && Route::has('login'))
                    <div class="nav-block">
                        <div class="nav-menu">
                            <a class="nav-link" href="{{ route('login') }}">Logowanie</a>
                        </div>
                        <div class="nav-menu">
                                <a class="nav-link" href="{{ route('register') }}">Rejestracja</a>
                        </div>
                    </div>
                        @endif
                    </div>
                @endauth
            @endif
    </div>
</div>

<div class="container-fluid">
    <nav class="navbar navbar-custom">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent15"
                aria-controls="navbarSupportedContent15" aria-expanded="false" aria-label="Toggle navigation">
            <span id="menu">Menu</span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent15">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="info">Strona główna</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="tickets">Bilety</a>
                </li>
                <li class="nav-item">
                    @auth
                        <a class="nav-link" href="create">Kup bilet</a>
                    @endauth
                    @guest
                            <a class="nav-link" href="login">Kup bilet</a>
                        @endguest
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="orders">Zamówienia</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div class="custom-fluid">
    @yield('content')
</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
</script>
<script src="{{ asset(('js/bootstrap.bundle.js')) }}"></script>
</html>
