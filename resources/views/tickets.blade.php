@extends('layouts.app')

@section('content')
<div class="custom-fluid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md text-col">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            @auth
                                <a href="create"><img class="d-block w-100 h-100" src="{{ asset('images/hhk.jpg') }}"
                                                     alt="First slide"></a>
                            @endauth

                            @guest
                            <a href="login"><img class="d-block w-100 h-100" src="{{ asset('images/hhk.jpg') }}"
                                                     alt="First slide"></a>
                                @endguest
                            <div class="carousel-caption d-none d-md-block">
                                <p>Bilety:</p>
                                <p>3-dniowy 315zł</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            @auth
                            <a href="login"><img class="d-block w-100 h-100" src="{{ asset('images/freshndope.jpg') }}"
                                                     alt="Second slide"></a>
                            @endauth
                            @guest
                                    <a href="login"><img class="d-block w-100 h-100" src="{{ asset('images/freshndope.jpg') }}"
                                                         alt="Second slide"></a>
                                @endguest
                            <div class="carousel-caption d-none d-md-block">
                                <p>Bilety:</p>
                                <p>Wszystkie koncerty 59zł</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            @auth
                            <a href="login"><img class="d-block w-100 h-100" src="{{ asset('images/gizycko.jpg') }}"
                                                     alt="Third slide"></a>
                            @endauth
                                @guest
                                    <a href="login"><img class="d-block w-100 h-100" src="{{ asset('images/gizycko.jpg') }}"
                                                         alt="Third slide"></a>
                                @endguest
                            <div class="carousel-caption d-none d-md-block">
                                <p class="black">Bilety:<br />3-dniowy 130zł</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                       data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                       data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
