@extends('layouts.app')

@section('content')
    @auth
        <div class="custom-fluid">
            <div class="custom-fluid">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md form-col">
                            <div class="custom-form-container">
                                <form role="form" action="{{ route('update', $order) }}" class="ticket-form" id="order-form" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="form-group form-group-custom" id="roles-box">
                                        <label for="inputFirstName" class="label-bold">Imię</label>
                                        <input type="text" value="{{ $order->firstname }}" name="firstname" class="form-control" id="inputFirstName">

                                        <label for="inputSecondName" class="label-bold">Nazwisko</label>
                                        <input type="text" value="{{ $order->secondname }}" name="secondname" class="form-control" id="inputSecondName">

                                        <label for="inputCity" class="label-bold">Miasto</label>
                                        <input type="text" value="{{ $order->city }}" name="city" class="form-control" id="inputCity">

                                        <label for="inputStreet" class="label-bold">Ulica</label>
                                        <input type="text" value="{{ $order->street }}" name="street" class="form-control" id="inputStreet">

                                        <label for="inputStreetNum" class="label-bold">Numer domu</label>
                                        <input type="text" value="{{ $order->streetnum }}" name="streetnum" class="form-control" id="inputStreetNum">

                                        <label for="inputTel" class="label-bold">Telefon</label>
                                        <input type="tel" value="{{ $order->telephone }}" name="telephone" class="form-control" id="inputTel">

                                        <label for="inputSelectFestival" class="label-bold">Bilet</label>
                                        <select class="form-control" value="{{ $order->festival }}" name="festival" id="inputSelectFestival">
                                            <option id="hhk">Hip-Hop Kemp</option>
                                            <option id="fnd">Fresh N Dope</option>
                                            <option id="hhg">Hip-Hop Festival Giżycko</option>
                                        </select>

                                        <label for="inputSelectQuantity" class="label-bold">Ilość</label>
                                        <select class="form-control" value="{{ $order->quantity }}" name="quantity" id="inputSelectQuantity">
                                            <option id="q1">1</option>
                                            <option id="q2">2</option>
                                            <option id="q3">3</option>
                                            <option id="q4">4</option>
                                            <option id="q5">5</option>
                                        </select>

                                        <label class="label-bold">Metoda płatności:</label><br />
                                        <input type="radio" class="form-check-input" name="payment" id="card"
                                               value="Karta płatnicza">
                                        <label for="card">Karta płatnicza</label><br />
                                        <input type="radio" class="form-check-input" name="payment" id="transfer"
                                               value="Przelew bankowy">
                                        <label for="transfer">Przelew bankowy</label><br />
                                        <input type="radio" class="form-check-input" name="payment" id="paypal"
                                               value="Paypal">
                                        <label for="paypal">Paypal</label>
                                        <br>

                                        <input type="checkbox" class="form-check-input" id="rules" name="rules" value="accepted">
                                        <label for="rules">Akceptuję regulamin serwisu i politykę prywatności.</label>
                                    </div>

                                    <button type="submit" class="btn btn-dark btn-success">Aktualizuj</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endauth
@endsection
