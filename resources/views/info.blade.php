@extends('layouts.app')

@section('content')
    <div class="custom-card">
        <p class="paragraph">
            Jesteśmy firmą zajmującą się sprzedażą biletów elektronicznych. Serwis zapewnia obsługę zamówień.
        </p>
        <p class="paragraph">
            Jeśli chcesz złozyć zamówienie, zaloguj/zarejestruj się.
        </p>
    </div>
@endsection
