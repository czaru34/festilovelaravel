@extends('layouts.app')

@section('content')
    <div class="custom-fluid">
        @auth
            <table data-toggle="table" class="table">
                <thead>
                    <tr>
                        <th class="thpurple" scope="col">Numer zamówienia</th>
                        <th class="thpurple" scope="col">Imię</th>
                        <th class="thpurple" scope="col">Nazwisko</th>
                        <th class="thpurple" scope="col">Miasto</th>
                        <th class="thpurple" scope="col">Ulica</th>
                        <th class="thpurple" scope="col">Numer domu</th>
                        <th class="thpurple" scope="col">Numer telefonu</th>
                        <th class="thpurple" scope="col">Bilet</th>
                        <th class="thpurple" scope="col">Ilość</th>
                        <th class="thpurple" scope="col">Płatność</th>
                        <th class="thpurple" scope="col"></th>
                        <th class="thpurple" scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->firstname }}</td>
                            <td>{{ $order->secondname }}</td>
                            <td>{{ $order->city }}</td>
                            <td>{{ $order->street }}</td>
                            <td>{{ $order->streetnum }}</td>
                            <td>{{ $order->telephone }}</td>
                            <td>{{ $order->festival }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->payment }}</td>
                            @if($order->userid == \Auth::user()->id)
                                <td>
                                    <a href="{{ route('edit', $order) }}" class="btn btn-dark btn-sm">Edytuj</a>
                                </td>
                                <td>
                                    <a href="{{ route('delete', $order) }}"
                                    class="btn btn-dark btn-sm"
                                    onclick="return confirm('Jesteś pewny usunięcia zamówienia?')">Anuluj</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @endauth
        @guest
            <div class="guest">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="login">Zaloguj się, aby widzieć zamówienia.</a>
                    </li>
                </ul>
            </div>
            @endguest
    </div>
@endsection
