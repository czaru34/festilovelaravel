<?php

Auth::routes();

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tickets', 'TicketsController@index');

Route::get('/', 'HomeController@index');

Route::get('/info', 'InfoController@index');

Route::get('/orders', ['middleware' => 'auth', 'uses' => 'OrdersController@index'])->name('orders');

Route::get('/create', 'OrdersController@create')->name('create');

Route::post('/create', 'OrdersController@store')->name('store');

Route::get('/delete/{id}', 'OrdersController@destroy')->name('delete');

Route::get('/edit/{id}', 'OrdersController@edit')->name('edit');
Route::put('{id}', 'OrdersController@update')->name('update');
