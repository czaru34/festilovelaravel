<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string|min:4|max:30',
            'secondname' => 'required|string|min:3|max:30',
            'city' => 'required|string|max:100',
            'street' => 'required|string|max:50',
            'streetnum' => 'required|string|max:3',
            'telephone' => 'required|integer',
            'festival' => 'required',
            'quantity' => 'required',
            'payment' => 'required',
            'rules' => 'required'
        ];
    }
}
