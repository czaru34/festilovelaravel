<?php

namespace App\Http\Controllers;
use App\Order;
use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    public function index()
    {
        if(!\Auth::guest()) {
            $orders = Order::where('userid', \Auth::user()->id)->get();
            return view('orders', compact('orders'));
        } else return view('orders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $order = new Order();
        return view('ordersForm', compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(OrderRequest $request)
    {
        if(\Auth::user() == null) return view('comments');
        $order = new Order();
        $order->userid = \Auth::user()->id;
        $order->firstname = $request->firstname;
        $order->secondname = $request->secondname;
        $order->city = $request->city;
        $order->street = $request->street;
        $order->streetnum = $request->streetnum;
        $order->telephone = $request->telephone;
        $order->festival = $request->festival;
        $order->quantity = $request->quantity;
        $order->payment = $request->payment;

        if ($order->save()) return redirect()->route('orders');
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $order = Order::find($id);
        if(\Auth::user()->id != $order->userid)
            return back()->with([
                'success' => false,
                'message_type' => 'danger',
                'message' => 'Nie masz takich uprawnień.'
            ]);
        return view('ordersEditForm', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        if(\Auth::user()->id != $order->userid)
            return back()->with([
                'success' => false,
                'message_type' => 'danger',
                'message' => 'Nie masz takich uprawnień.'
            ]);
        $order->firstname = $request->firstname;
        $order->secondname = $request->secondname;
        $order->city = $request->city;
        $order->street = $request->street;
        $order->streetnum = $request->streetnum;
        $order->telephone = $request->telephone;
        $order->festival = $request->festival;
        $order->quantity = $request->quantity;
        $order->payment = $request->payment;
        if($order->save())
            return redirect()->route('orders');
        return "Błąd edycji. Spróbuj później.";
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if(\Auth::user()->id != $order->userid)
            return back()->with([
                'success' => false,
                'message_type' => 'danger',
                'message' => 'Nie masz takich uprawnień.'
            ]);

        if($order->delete())
            return redirect()->route('orders')->with([
                'success' => 'true',
                'message_type' => 'success',
                'message' => "Usunięto zamówienie."
            ]);

        return back()->with([
            'success' => false,
            'message_type' => 'danger',
            'message' => 'Wystapił błąd podczas usuwania zamówienia. Spróbuj później.'
        ]);
    }
}
